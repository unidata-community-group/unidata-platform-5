/**
 * formFields для поиска по атрибутам связей
 *
 * @author Aleksandr Bavin
 * @date 2018-02-13
 */
Ext.define('Unidata.module.search.term.relation.FormField', {

    extend: 'Unidata.module.search.term.FormField',

    config: {
        type: Unidata.module.search.term.FormFieldStatics.type.STRING
    }

});
