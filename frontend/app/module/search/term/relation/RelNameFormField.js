/**
 * formFields для поиска по атрибутам связей
 *
 * @author Aleksandr Bavin
 * @date 2018-02-13
 */
Ext.define('Unidata.module.search.term.relation.RelNameFormField', {

    extend: 'Unidata.module.search.term.relation.FormField',

    config: {
        name: '$rel_name'
    }

});
