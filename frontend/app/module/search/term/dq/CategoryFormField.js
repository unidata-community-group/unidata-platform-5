/**
 * категория dq
 *
 * @author Aleksandr Bavin
 * @date 2018-02-13
 */
Ext.define('Unidata.module.search.term.dq.CategoryFormField', {

    extend: 'Unidata.module.search.term.dq.FormField',

    termName: 'dq.category',

    config: {
        name: '$dq_errors.category'
    }

});
