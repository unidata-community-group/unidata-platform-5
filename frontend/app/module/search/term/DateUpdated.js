/**
 * Обновлена
 *
 * @author Aleksandr Bavin
 * @date 2018-02-12
 */
Ext.define('Unidata.module.search.term.DateUpdated', {

    extend: 'Unidata.module.search.term.DateAbstract',

    termName: 'dateUpdated',

    name: '$updated_at'

});
