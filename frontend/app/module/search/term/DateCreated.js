/**
 * Создана
 *
 * @author Aleksandr Bavin
 * @date 2018-02-12
 */
Ext.define('Unidata.module.search.term.DateCreated', {

    extend: 'Unidata.module.search.term.DateAbstract',

    termName: 'dateCreated',

    name: '$created_at'

});
