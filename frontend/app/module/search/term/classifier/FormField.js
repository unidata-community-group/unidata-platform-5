/**
 * formFields для поиска по атрибутам классификатора
 *
 * @author Aleksandr Bavin
 * @date 2018-02-13
 */
Ext.define('Unidata.module.search.term.classifier.FormField', {

    extend: 'Unidata.module.search.term.FormField'

    // value - classifierNode id
    // name - classifierName.$nodes.$node_id

});
