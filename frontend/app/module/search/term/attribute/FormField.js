/**
 * formFields для поиска по атрибутам модели
 *
 * @author Aleksandr Bavin
 * @date 2018-02-13
 */
Ext.define('Unidata.module.search.term.attribute.FormField', {

    extend: 'Unidata.module.search.term.FormField'

});
