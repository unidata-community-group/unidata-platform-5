Ext.define('Unidata.model.cleansefunction.IfThenElsePort', {
    extend: 'Unidata.model.Base',

    idProperty: 'id',

    fields: [
        {
            name: 'dataType', type: 'string'
        }
    ]
});
