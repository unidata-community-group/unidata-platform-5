/**
 * @author Aleksandr Bavin
 * @date 2016-08-25
 */

Ext.define('Unidata.view.workflow.task.panels.HistoryPanelController', {
    extend: 'Unidata.view.workflow.task.panels.AbstractPanelController',

    alias: 'controller.history'
});
