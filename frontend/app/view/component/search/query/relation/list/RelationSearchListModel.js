Ext.define('Unidata.view.component.search.query.relation.list.RelationSearchListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.component.search.query.relation.list.relationsearchlist',

    data: {
        addRelationSearchItemButtonEnabled: false
    },

    formulas: {
    }
});
