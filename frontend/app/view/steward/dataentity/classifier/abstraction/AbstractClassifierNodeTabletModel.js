/**
 * Абстрактный компонент представления коллекции узлов классификации
 * @author Sergey Shishigin
 * @date 2018-05-031
 */
Ext.define('Unidata.view.steward.dataentity.classifier.abstraction.AbstractClassifierNodeTabletModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.steward.dataentity.classifiernode.abstractclassifiernodetablet',

    data: {
        readOnly: null
    }
});
