/**
 *
 * @author Ivan Marshalkin
 * @date 2018-01-29
 */

Ext.define('Unidata.view.admin.entity.metarecord.dq.DataQualityModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.admin.entity.metarecord.dq',

    data: {
        readOnly: null
    },

    stores: {},

    formulas: {}
});
