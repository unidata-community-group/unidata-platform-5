/**
 * Переопределение конфигурации для локализации
 *
 * @author Ivan Marshalkin
 * @date 2018-05-29
 */

Ext.define('Ext.locale.ru.form.RadioGroup', {
    override: 'Ext.form.RadioGroup',

    blankText: 'Выберите один элемент в этой группе'
});
