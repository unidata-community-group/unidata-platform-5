/**
 * Переопределение конфигурации для локализации
 *
 * @author Ivan Marshalkin
 * @date 2018-05-29
 */

Ext.define('Ext.locale.ru.form.CheckboxGroup', {
    override: 'Ext.form.CheckboxGroup',

    blankText: 'Выберите один или несколько элементов в этой группе'
});
