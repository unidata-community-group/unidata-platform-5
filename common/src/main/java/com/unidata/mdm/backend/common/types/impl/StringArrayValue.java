/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.unidata.mdm.backend.common.types.impl;

import com.unidata.mdm.backend.common.types.CodeLinkValue;

/**
 * @author Mikhail Mikhailov
 * String array value.
 */
public class StringArrayValue extends AbstractArrayValue<String> implements CodeLinkValue {

    /**
     * Link etalon id.
     */
    private String linkEtalonId;
    /**
     * Constructor.
     */
    public StringArrayValue() {
        super();
    }
    /**
     * Constructor.
     * @param value
     * @param displayValue
     */
    public StringArrayValue(String value, String displayValue) {
        super(value, displayValue);
    }
    /**
     * Constructor.
     * @param value the value
     * @param displayValue the display value
     * @param linkEtalonId link etalon id
     */
    public StringArrayValue(String value, String displayValue, String linkEtalonId) {
        super(value, displayValue);
        this.linkEtalonId = linkEtalonId;
    }
    /**
     * Constructor.
     * @param value
     */
    public StringArrayValue(String value) {
        super(value);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getLinkEtalonId() {
        return linkEtalonId;
    }
    /**
     * @param linkEtalonId the linkEtalonId to set
     */
    @Override
    public void setLinkEtalonId(String linkEtalonId) {
        this.linkEtalonId = linkEtalonId;
    }

}
