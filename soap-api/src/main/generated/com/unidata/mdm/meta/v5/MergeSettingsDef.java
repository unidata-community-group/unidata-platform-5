
package com.unidata.mdm.meta.v5;

import java.io.Serializable;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.cxf.xjc.runtime.JAXBToStringStyle;


/**
 * <p>Java class for MergeSettingsDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MergeSettingsDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bvtSettings" type="{http://meta.mdm.unidata.com/v5/}BVTMergeTypeDef" minOccurs="0"/&gt;
 *         &lt;element name="bvrSettings" type="{http://meta.mdm.unidata.com/v5/}BVRMergeTypeDef" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MergeSettingsDef", propOrder = {
    "bvtSettings",
    "bvrSettings"
})
@Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
public class MergeSettingsDef
    implements Serializable
{

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    private final static long serialVersionUID = 12345L;
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    protected BVTMergeTypeDef bvtSettings;
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    protected BVRMergeTypeDef bvrSettings;

    /**
     * Gets the value of the bvtSettings property.
     * 
     * @return
     *     possible object is
     *     {@link BVTMergeTypeDef }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    public BVTMergeTypeDef getBvtSettings() {
        return bvtSettings;
    }

    /**
     * Sets the value of the bvtSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link BVTMergeTypeDef }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    public void setBvtSettings(BVTMergeTypeDef value) {
        this.bvtSettings = value;
    }

    /**
     * Gets the value of the bvrSettings property.
     * 
     * @return
     *     possible object is
     *     {@link BVRMergeTypeDef }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    public BVRMergeTypeDef getBvrSettings() {
        return bvrSettings;
    }

    /**
     * Sets the value of the bvrSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link BVRMergeTypeDef }
     *     
     */
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    public void setBvrSettings(BVRMergeTypeDef value) {
        this.bvrSettings = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    public MergeSettingsDef withBvtSettings(BVTMergeTypeDef value) {
        setBvtSettings(value);
        return this;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    public MergeSettingsDef withBvrSettings(BVRMergeTypeDef value) {
        setBvrSettings(value);
        return this;
    }

    /**
     * Generates a String representation of the contents of this type.
     * This is an extension method, produced by the 'ts' xjc plugin
     * 
     */
    @Override
    @Generated(value = "com.sun.tools.xjc.Driver", date = "2020-06-05T12:30:23+03:00", comments = "JAXB RI v2.2.11")
    public String toString() {
        return ToStringBuilder.reflectionToString(this, JAXBToStringStyle.DEFAULT_STYLE);
    }

}
