
/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

package com.unidata.mdm.dq.api.v5;

import java.util.logging.Logger;
import javax.annotation.Generated;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.1.0
 * 2020-06-05T12:30:16.683+03:00
 * Generated source version: 3.1.0
 * 
 */

@javax.jws.WebService(
                      serviceName = "DataQualitySOAPService",
                      portName = "applyDQ",
                      targetNamespace = "http://api.dq.mdm.unidata.com/v5/",
                      wsdlLocation = "classpath:api/v5/dq-unidata-api.wsdl",
                      endpointInterface = "com.unidata.mdm.dq.api.v5.DataQualityPortType")
                      
@Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:16.683+03:00", comments = "Apache CXF 3.1.0")
public class ApplyDQImpl implements DataQualityPortType {

    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:16.683+03:00")
    private static final Logger LOG = Logger.getLogger(ApplyDQImpl.class.getName());

    /* (non-Javadoc)
     * @see com.unidata.mdm.dq.api.v5.DataQualityPortType#applyDQ(com.unidata.mdm.dq.v5.ApplyDQRequest  applyDQRequest ,)com.unidata.mdm.api.v5.SessionTokenDef  security ,)com.unidata.mdm.dq.v5.InfoType  info )*
     */
    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:16.683+03:00")
    public com.unidata.mdm.dq.v5.ApplyDQResponse applyDQ(com.unidata.mdm.dq.v5.ApplyDQRequest applyDQRequest,javax.xml.ws.Holder<com.unidata.mdm.api.v5.SessionTokenDef> security,javax.xml.ws.Holder<com.unidata.mdm.dq.v5.InfoType> info) throws ApiFault    { 
        LOG.info("Executing operation applyDQ");
        System.out.println(applyDQRequest);
        System.out.println(security.value);
        System.out.println(info.value);
        try {
            com.unidata.mdm.dq.v5.ApplyDQResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ApiFault("ApiFault...");
    }

    /* (non-Javadoc)
     * @see com.unidata.mdm.dq.api.v5.DataQualityPortType#getResults(com.unidata.mdm.dq.v5.ResultsRequestType  getResultsRequest ,)com.unidata.mdm.api.v5.SessionTokenDef  security ,)com.unidata.mdm.dq.v5.InfoType  info )*
     */
    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:16.683+03:00")
    public com.unidata.mdm.dq.v5.ResultsResponseType getResults(com.unidata.mdm.dq.v5.ResultsRequestType getResultsRequest,javax.xml.ws.Holder<com.unidata.mdm.api.v5.SessionTokenDef> security,javax.xml.ws.Holder<com.unidata.mdm.dq.v5.InfoType> info) throws ApiFault    { 
        LOG.info("Executing operation getResults");
        System.out.println(getResultsRequest);
        System.out.println(security.value);
        System.out.println(info.value);
        try {
            com.unidata.mdm.dq.v5.ResultsResponseType _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new ApiFault("ApiFault...");
    }

}
