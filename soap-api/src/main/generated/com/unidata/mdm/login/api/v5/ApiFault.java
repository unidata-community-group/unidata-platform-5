
package com.unidata.mdm.login.api.v5;

import javax.annotation.Generated;
import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.1.0
 * 2020-06-05T12:30:20.229+03:00
 * Generated source version: 3.1.0
 */

@WebFault(name = "ApiFault", targetNamespace = "http://error-handling.mdm.unidata.com/v5/")
@Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:20.229+03:00", comments = "Apache CXF 3.1.0")
public class ApiFault extends Exception {
    
    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:20.229+03:00")
    private com.unidata.mdm.error_handling.v5.ApiFaultType apiFault;

    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:20.229+03:00")
    public ApiFault() {
        super();
    }
    
    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:20.229+03:00")
    public ApiFault(String message) {
        super(message);
    }
    
    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:20.229+03:00")
    public ApiFault(String message, Throwable cause) {
        super(message, cause);
    }

    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:20.229+03:00")
    public ApiFault(String message, com.unidata.mdm.error_handling.v5.ApiFaultType apiFault) {
        super(message);
        this.apiFault = apiFault;
    }

    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:20.229+03:00")
    public ApiFault(String message, com.unidata.mdm.error_handling.v5.ApiFaultType apiFault, Throwable cause) {
        super(message, cause);
        this.apiFault = apiFault;
    }

    @Generated(value = "org.apache.cxf.tools.wsdlto.WSDLToJava", date = "2020-06-05T12:30:20.229+03:00")
    public com.unidata.mdm.error_handling.v5.ApiFaultType getFaultInfo() {
        return this.apiFault;
    }
}
