//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.09.09 at 03:48:37 PM MSK 
//


package com.unidata.mdm.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StatisticEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StatisticEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NEW"/&gt;
 *     &lt;enumeration value="ERRORS"/&gt;
 *     &lt;enumeration value="UPDATED"/&gt;
 *     &lt;enumeration value="TOTAL"/&gt;
 *     &lt;enumeration value="MERGED"/&gt;
 *     &lt;enumeration value="DUPLICATES"/&gt;
 *     &lt;enumeration value="CLUSTERS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "StatisticEnum")
@XmlEnum
public enum StatisticEnum {

    NEW,
    ERRORS,
    UPDATED,
    TOTAL,
    MERGED,
    DUPLICATES,
    CLUSTERS;

    public String value() {
        return name();
    }

    public static StatisticEnum fromValue(String v) {
        return valueOf(v);
    }

}
