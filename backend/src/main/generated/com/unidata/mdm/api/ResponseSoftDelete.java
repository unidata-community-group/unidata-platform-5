//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.09.09 at 03:48:37 PM MSK 
//


package com.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.unidata.mdm.data.EtalonKey;
import com.unidata.mdm.data.OriginKey;


/**
 * 
 *                 Ответ на запрос на логическое удаление основной или исходной записи сущности ('RequestSoftDelete').
 *                 Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках
 *                 (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * 
 *                 В случае успешного исполнения содержит список ключей логически удалённых записей, а также
 *                 соответствующих исходных записей.
 *             
 * 
 * <p>Java class for ResponseSoftDelete complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseSoftDelete"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.com/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="etalonKeys" type="{http://data.mdm.unidata.com/}EtalonKey" maxOccurs="unbounded"/&gt;
 *         &lt;element name="originKeys" type="{http://data.mdm.unidata.com/}OriginKey" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseSoftDelete", propOrder = {
    "etalonKeys",
    "originKeys"
})
public class ResponseSoftDelete
    extends UnidataAbstractResponse
    implements Serializable
{

    private final static long serialVersionUID = 12345L;
    @XmlElement(required = true)
    protected List<EtalonKey> etalonKeys;
    @XmlElement(required = true)
    protected List<OriginKey> originKeys;

    /**
     * Gets the value of the etalonKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etalonKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEtalonKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EtalonKey }
     * 
     * 
     */
    public List<EtalonKey> getEtalonKeys() {
        if (etalonKeys == null) {
            etalonKeys = new ArrayList<EtalonKey>();
        }
        return this.etalonKeys;
    }

    /**
     * Gets the value of the originKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the originKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginKey }
     * 
     * 
     */
    public List<OriginKey> getOriginKeys() {
        if (originKeys == null) {
            originKeys = new ArrayList<OriginKey>();
        }
        return this.originKeys;
    }

    public ResponseSoftDelete withEtalonKeys(EtalonKey... values) {
        if (values!= null) {
            for (EtalonKey value: values) {
                getEtalonKeys().add(value);
            }
        }
        return this;
    }

    public ResponseSoftDelete withEtalonKeys(Collection<EtalonKey> values) {
        if (values!= null) {
            getEtalonKeys().addAll(values);
        }
        return this;
    }

    public ResponseSoftDelete withOriginKeys(OriginKey... values) {
        if (values!= null) {
            for (OriginKey value: values) {
                getOriginKeys().add(value);
            }
        }
        return this;
    }

    public ResponseSoftDelete withOriginKeys(Collection<OriginKey> values) {
        if (values!= null) {
            getOriginKeys().addAll(values);
        }
        return this;
    }

}
