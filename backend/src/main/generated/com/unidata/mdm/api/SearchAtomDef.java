//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.09.09 at 03:48:37 PM MSK 
//


package com.unidata.mdm.api;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * Структура, описывающая условия поиска на конкретный атрибут сущности. Используется для формирования условий поиска сущности в запросе 'RequestSearch'.
 * Всегда состоит из имени атрибута, оператора сравнения и константы.
 *             
 * 
 * <p>Java class for SearchAtomDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchAtomDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.com/}SearchBaseDef"&gt;
 *       &lt;attribute name="attributeName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="operator" use="required" type="{http://api.mdm.unidata.com/}CompareOperatorType" /&gt;
 *       &lt;attribute name="constant" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchAtomDef")
public class SearchAtomDef
    extends SearchBaseDef
    implements Serializable
{

    private final static long serialVersionUID = 12345L;
    @XmlAttribute(name = "attributeName", required = true)
    protected String attributeName;
    @XmlAttribute(name = "operator", required = true)
    protected CompareOperatorType operator;
    @XmlAttribute(name = "constant", required = true)
    protected String constant;

    /**
     * Gets the value of the attributeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Sets the value of the attributeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeName(String value) {
        this.attributeName = value;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link CompareOperatorType }
     *     
     */
    public CompareOperatorType getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompareOperatorType }
     *     
     */
    public void setOperator(CompareOperatorType value) {
        this.operator = value;
    }

    /**
     * Gets the value of the constant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstant() {
        return constant;
    }

    /**
     * Sets the value of the constant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstant(String value) {
        this.constant = value;
    }

    public SearchAtomDef withAttributeName(String value) {
        setAttributeName(value);
        return this;
    }

    public SearchAtomDef withOperator(CompareOperatorType value) {
        setOperator(value);
        return this;
    }

    public SearchAtomDef withConstant(String value) {
        setConstant(value);
        return this;
    }

}
