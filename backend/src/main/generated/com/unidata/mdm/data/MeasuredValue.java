//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.09.09 at 03:48:37 PM MSK 
//


package com.unidata.mdm.data;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MeasuredValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeasuredValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.com/}BaseValue"&gt;
 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="measurementValueId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="measurementUnitId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeasuredValue")
public class MeasuredValue
    extends BaseValue
    implements Serializable
{

    private final static long serialVersionUID = 12345L;
    @XmlAttribute(name = "value")
    protected Double value;
    @XmlAttribute(name = "measurementValueId", required = true)
    protected String measurementValueId;
    @XmlAttribute(name = "measurementUnitId")
    protected String measurementUnitId;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * Gets the value of the measurementValueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementValueId() {
        return measurementValueId;
    }

    /**
     * Sets the value of the measurementValueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementValueId(String value) {
        this.measurementValueId = value;
    }

    /**
     * Gets the value of the measurementUnitId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementUnitId() {
        return measurementUnitId;
    }

    /**
     * Sets the value of the measurementUnitId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementUnitId(String value) {
        this.measurementUnitId = value;
    }

    public MeasuredValue withValue(Double value) {
        setValue(value);
        return this;
    }

    public MeasuredValue withMeasurementValueId(String value) {
        setMeasurementValueId(value);
        return this;
    }

    public MeasuredValue withMeasurementUnitId(String value) {
        setMeasurementUnitId(value);
        return this;
    }

}
