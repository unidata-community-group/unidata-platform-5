/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package com.unidata.mdm.cleanse.common;

/**
 * Class contains constants used across basic cleanse functions implementation.
 * @author ilya.bykov
 */
public class CleanseConstants {

	/** The Constant INPUT1. */
	public static final String INPUT1 = "port1";

	/** The Constant INPUT2. */
	public static final String INPUT2 = "port2";
	/** The Constant INPUT3. */
	public static final String INPUT3 = "port3";
	/** The Constant INPUT4. */
	public static final String INPUT4 = "port4";
	/** The Constant OUTPUT1. */
	public static final String OUTPUT1 = "port1";
	/** The Constant OUTPUT2. */
	public static final String OUTPUT2 = "port2";
	/** The Constant OUTPUT3. */
    public static final String OUTPUT3 = "port3";

	/** The Constant DEFAULT_VALUE. */
	public static final String DEFAULT_VALUE = "port2";

	/** The Constant PAD_LEFT. */
	public static final String PAD_LEFT = "port2";

	/** The Constant PAD_RIGHT. */
	public static final String PAD_RIGHT = "port2";

	/** The Constant PATTERN. */
	public static final String PATTERN = "port2";

	/** The Constant GROUP. */
	public static final String GROUP = "port3";

	/** The Constant START_INDEX. */
	public static final String START_INDEX = "port2";

	/** The Constant END_INDEX. */
	public static final String END_INDEX = "port3";
}
